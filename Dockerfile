FROM rocker/shiny:4.1.0
LABEL maintainer="Salma Dilai <salma.dilai@mpi-bn.mpg.de>"
LABEL version="0.1"

RUN \
    # Assure up-to-date package lists  
    apt-get update && \
    ##Assure up-to-date environment
    apt-get dist-upgrade --assume-yes && \
    apt-get install --assume-yes --no-install-recommends \
      ## Fetch additional libraries
      libssh2-1-dev libgit2-dev libxml2-dev libssl-dev \
      ## Dependencies of multipanelfigure (etc.)
      libmagick++-dev \
      ## Fetch the additional tools
      git && \
    # Install pandoc
    mkdir -p /pandoc_deb  && \
	  wget -O /pandoc_deb/pandoc-2.9.2.1-1-amd64.deb https://github.com/jgm/pandoc/releases/download/2.9.2.1/pandoc-2.9.2.1-1-amd64.deb && \
	  sudo dpkg -i /pandoc_deb/pandoc-2.9.2.1-1-amd64.deb && \
    # Clean the cache(s)
    apt-get clean && \
    rm -r /var/lib/apt/lists/* && \
    # Install R infrastructure
    Rscript -e 'source("https://gitlab.gwdg.de/loosolab/container/tobias_i2dash/-/raw/main/setup.R")' && \
    # Set the time zone
    sh -c "echo 'Europe/Berlin' > /etc/timezone" && \
    dpkg-reconfigure -f noninteractive tzdata && \
    # Change ownerships
    chmod -R ugo+wrX /var/log/shiny-server/ && \
    chown -R shiny:shiny /srv/shiny-server/ && \
    # Clean up existing sample apps
    rm -rf /srv/shiny-server/[0-9][0-9]_* /srv/shiny-server/index.html /srv/shiny-server/sample-apps

CMD ["R", "-e", "rmarkdown::run(file=NULL,dir='/srv/shiny-server',shiny_args=list(port = 3838, host = '0.0.0.0'))"]
