## Tobias_I2dash_Diagram

![Tobias_I2dash_Diagram](/uploads/e3aaea6a13dbe06bb1ae217a885402fc/Tobias_I2dash_Diagram.jpg)

### To run the R script from the terminal:
1. we need to create a conda environemt with all the dependecies required:  copy paste this yaml file in your VM , "conda_tobias_i2dash_env.yaml", in your terminal:    
`$ conda env create -f conda_tobias_i2dash_env.yaml`

2. activate the conda environment:             
`$ conda activate tobias_i2dash_env`

3. Run the R script:        
`$ Rscript Tobias_I2dash.R -i <the path of the tobias run> -o <The path where you want to have the ouput>`

** example:`$ Rscript Tobias_I2dash.R -i /home/docker_containers/Tcell_Bcell_project/ -o /home/docker_containers/Tcell_Output`

4. In the output folder, there will be the "pictures" folder containing all the graphs and pictures of the TFs, in addition to the Rmd file.

5. Creation of Docker container: 

- 6.1. Build the docker image in your vm:      
`$ docker build -t docker.gitlab.gwdg.de/loosolab/container/tobias_i2dash:1.0 https://gitlab.gwdg.de/loosolab/container/tobias_i2dash.git#main`
  
- 6.2. Run the container:     
`$ docker run -d -p <IP>:3838:3838 -v <path/to/output_folder_containing_rmd>:/srv/shiny-server docker.gitlab.gwdg.de/loosolab/container/tobias_i2dash:1.0`

7. In your browser: \<IP\>:3838 (e.g. IP=172.16.12.174), and you can visualize your dashboard!
